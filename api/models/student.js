const mongoose = require('mongoose');
const studentSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    name: String,
    age: String,
    address: String,
    birthDate: String,
    enrollmentNo: String,
    parentName: String,
    contactNo: String,
    gender: String,
    image: String,
    isimage: Boolean
   
});

module.exports = mongoose.model('Students', studentSchema);