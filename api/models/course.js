const mongoose = require('mongoose');
const courseSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    courseCode:String,
    title:String,
    image: String,
    isimage: Boolean

});

module.exports = mongoose.model('Courses', courseSchema);