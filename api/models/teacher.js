const mongoose = require('mongoose');
const teacherSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    name: String,
    age: String,
    address: String,
    birthDate: String,
    contactNo: String,
    gender: String,
    image: String,
    isimage: Boolean
});

module.exports = mongoose.model('Teachers', teacherSchema);