const mongoose = require('mongoose');
const branchSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    name:String,
    address:String,
    contactNo:String,
    image: String,
    isimage: Boolean
});

module.exports = mongoose.model('Branches', branchSchema);