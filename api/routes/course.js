const express = require('express');
const Courses = require('../models/course');
const router = express.Router();
const mongoose = require('mongoose');
const multer = require('multer');
const checkAuth = require('../middleware/check-auth');

const storage = multer.diskStorage({
    destination: function(req, file, cb){
        cb(null, './uploads/course')
    },
    filename: function(req, file, cb){
        cb(null, new Date().toISOString().replace(/:/g, '-')+file.originalname);
    }
})
 
const filefilter = (req, file ,cb) =>{
    if(file.mimetype ==='image/jpeg' || file.mimetype ==='image/png'){
        cb(null, true); 
    }else{
        cb(null, false); 
    }    
}

const upload = multer({ 
    storage:storage ,
    limits:{
    fileSize: 1024 * 1024 * 5
    },
    fileFilter:filefilter
})

router.get('/', (req, res, next)=>{
    Courses.find().exec()
    .then(doc=>{

        res.status(200).json(doc);
    }).catch(err=>{
        res.status(500).json({
            error: err
        })
    })
})

router.post('/', checkAuth, upload.single('imagefile'), (req, res, next)=>{
   
    var filepath = null
    if(req.body.isimage){
        filepath = req.file.path
    }
    const courses = new Courses({
        _id: new mongoose.Types.ObjectId(),
        courseCode:req.body.courseCode,
        title:req.body.title,
        isimage: req.body.isimage,
        image: filepath,
        isimage: req.body.isimage,
        image: filepath
        
    })
    courses.save().then(result=>{

        res.status(200).json({
            message: 'post handler success',
            recivedname:courses.name
        })
    }).catch(err=>{
        res.status(500).json({
            error:err
        })
    });
})

router.get('/:courseId', (req, res, next)=>{
    const id = req.params.courseId;
    Courses.findById(id).exec().then(doc=>{
        if(doc){
            res.status(200).json(doc)
        }else{
            res.status(500).json({error: "ERROR: ID Not found"})
        }
    }).catch(err =>{ 
        res.status(500).json({error: err})
    });
})



router.patch("/:courseId", checkAuth, upload.single('imagefile'), (req, res, next)=>{
    const id = req.params.courseId;
    var filepath = null
    if(req.body.isimage){
        filepath = req.file.path
    }
    const updateOps = {
        courseCode:req.body.courseCode,
        title:req.body.title,
        isimage: req.body.isimage,
        image: filepath
    };
    Categories.updateOne({_id:id}, {$set: updateOps}).exec().then(result =>{
        res.status(200).json(result);
    }).catch(err=>{
        res.status(500).json({
            error:err
        })
    })
});

router.delete("/:courseId", (req, res, next)=>{
    const id = req.params.courseId;
    Courses.deleteOne({_id:id}).exec().then(result=>{
        res.status(200).json(result); 
    }).catch(err=>{
        res.status(500).json({
            error:err
        });
    });

});

module.exports = router;