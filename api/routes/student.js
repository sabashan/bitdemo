const express = require('express');
const Student = require('../models/student');
const router = express.Router();
const mongoose = require('mongoose');
const multer = require('multer');
const checkAuth = require('../middleware/check-auth')

const storage = multer.diskStorage({
    destination: function(req, file, cb){
        cb(null, './uploads/student')
    },
    filename: function(req, file, cb){
        cb(null, new Date().toISOString().replace(/:/g, '-')+file.originalname);
    }
})
 
const filefilter = (req, file ,cb) =>{
    if(file.mimetype ==='image/jpeg' || file.mimetype ==='image/png'){
        cb(null, true); 
    }else{
        cb(null, false); 
    }    
}

const upload = multer({ 
    storage:storage ,
    limits:{
    fileSize: 1024 * 1024 * 5
    },
    fileFilter:filefilter
})

router.get('/', (req, res, next)=>{
    Student.find().exec()
    .then(doc=>{

        res.status(200).json(doc);
    }).catch(err=>{
        res.status(500).json({
            error: err
        })
    })
})

router.post('/', checkAuth, upload.single('imagefile'), (req, res, next)=>{
   
    var filepath = null
    if(req.body.isimage){
        filepath = req.file.path
    }
    const students = new Student({
        _id: new mongoose.Types.ObjectId(),
        name: req.body.name,
        age: req.body.age,
        address: req.body.address,
        birthDate: req.body.birthDate,
        enrollmentNo: req.body.enrollmentNo,
        parentName: req.body.parentName,
        contactNo: req.body.contactNo,
        gender: req.body.gender,
        image: filepath,
        isimage: req.body.isimage
        
        
    })
    students.save().then(result=>{

        res.status(200).json({
            message: 'post handler success',
            recivedname:students.name
        })
    }).catch(err=>{
        res.status(500).json({
            error:err
        })
    });
})

router.get('/:studentId', (req, res, next)=>{
    const id = req.params.studentId;
    Student.findById(id).exec().then(doc=>{
        if(doc){
            res.status(200).json(doc)
        }else{
            res.status(500).json({error: "ERROR: ID Not found"})
        }
    }).catch(err =>{ 
        res.status(500).json({error: err})
    });
})



router.patch("/:studentId", checkAuth, upload.single('imagefile'), (req, res, next)=>{
    const id = req.params.studentId;
    var filepath = null
    if(req.body.isimage){
        filepath = req.file.path
    }
    const updateOps = {
        name: req.body.name,
        age: req.body.age,
        address: req.body.address,
        birthDate: req.body.birthDate,
        enrollmentNo: req.body.enrollmentNo,
        parentName: req.body.parentName,
        contactNo: req.body.contactNo,
        gender: req.body.gender,
        image: filepath,
        isimage: req.body.isimage
        
    };
    Categories.updateOne({_id:id}, {$set: updateOps}).exec().then(result =>{
        res.status(200).json(result);
    }).catch(err=>{
        res.status(500).json({
            error:err
        })
    })
});

router.delete("/:studentId", (req, res, next)=>{
    const id = req.params.studentId;
    Student.deleteOne({_id:id}).exec().then(result=>{
        res.status(200).json(result); 
    }).catch(err=>{
        res.status(500).json({
            error:err
        });
    });

});

module.exports = router;