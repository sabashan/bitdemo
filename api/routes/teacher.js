const express = require('express');
const Teachers = require('../models/teacher');
const router = express.Router();
const mongoose = require('mongoose');
const multer = require('multer');
const checkAuth = require('../middleware/check-auth');

const storage = multer.diskStorage({
    destination: function(req, file, cb){
        cb(null, './uploads/teacher')
    },
    filename: function(req, file, cb){
        cb(null, new Date().toISOString().replace(/:/g, '-')+file.originalname);
    }
})
 
const filefilter = (req, file ,cb) =>{
    if(file.mimetype ==='image/jpeg' || file.mimetype ==='image/png'){
        cb(null, true); 
    }else{
        cb(null, false); 
    }    
}

const upload = multer({ 
    storage:storage ,
    limits:{
    fileSize: 1024 * 1024 * 5
    },
    fileFilter:filefilter
})

router.get('/', (req, res, next)=>{
    Teachers.find().exec()
    .then(doc=>{

        res.status(200).json(doc);
    }).catch(err=>{
        res.status(500).json({
            error: err
        })
    })
})

router.post('/', checkAuth, upload.single('imagefile'), (req, res, next)=>{
    
    var filepath = null
    if(req.body.isimage){
        filepath = req.file.path
    }
    const teachers = new Teachers({
        _id: new mongoose.Types.ObjectId(),
        name: req.body.name,
        age: req.body.age,
        address: req.body.address,
        birthDate: req.body.birthDate,
        contactNo: req.body.contactNo,
        gender: req.body.gender,
        image: filepath,
        isimage: req.body.isimage
        
        
    })
    teachers.save().then(result=>{

        res.status(200).json({
            message: 'post handler success',
            recivedname:teachers.name
        })
    }).catch(err=>{
        res.status(500).json({
            error:err
        })
    });
})

router.get('/:teacherId', (req, res, next)=>{
    const id = req.params.teacherId;
    Teachers.findById(id).exec().then(doc=>{
        if(doc){
            res.status(200).json(doc)
        }else{
            res.status(500).json({error: "ERROR: ID Not found"})
        }
    }).catch(err =>{ 
        res.status(500).json({error: err})
    });
})



router.patch("/:teacherId", checkAuth, upload.single('imagefile'), (req, res, next)=>{
    const id = req.params.teacherId;
    var filepath = null
    if(req.body.isimage){
        filepath = req.file.path
    }
    const updateOps = {
        name: req.body.name,
        age: req.body.age,
        address: req.body.address,
        birthDate: req.body.birthDate,
        contactNo: req.body.contactNo,
        gender: req.body.gender,
        image: filepath,
        isimage: req.body.isimage,
        image: filepath
        
    };
    Categories.updateOne({_id:id}, {$set: updateOps}).exec().then(result =>{
        res.status(200).json(result);
    }).catch(err=>{
        res.status(500).json({
            error:err
        })
    })
});

router.delete("/:teacherId", (req, res, next)=>{
    const id = req.params.teacherId;
    Teachers.deleteOne({_id:id}).exec().then(result=>{
        res.status(200).json(result); 
    }).catch(err=>{
        res.status(500).json({
            error:err
        });
    });

});

module.exports = router;