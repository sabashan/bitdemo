const express = require('express');
const Branches = require('../models/branch');
const router = express.Router();
const mongoose = require('mongoose');
const multer = require('multer');
const checkAuth = require('../middleware/check-auth');

const storage = multer.diskStorage({
    destination: function(req, file, cb){
        cb(null, './uploads/branch')
    },
    filename: function(req, file, cb){
        cb(null, new Date().toISOString().replace(/:/g, '-')+file.originalname);
    }
})
 
const filefilter = (req, file ,cb) =>{
    if(file.mimetype ==='image/jpeg' || file.mimetype ==='image/png'){
        cb(null, true); 
    }else{
        cb(null, false); 
    }    
}

const upload = multer({ 
    storage:storage ,
    limits:{
    fileSize: 1024 * 1024 * 5
    },
    fileFilter:filefilter
})

router.get('/', (req, res, next)=>{
    Branches.find().exec()
    .then(doc=>{

        res.status(200).json(doc);
    }).catch(err=>{
        res.status(500).json({
            error: err
        })
    })
})

router.post('/', checkAuth, upload.single('imagefile'), (req, res, next)=>{
    
    var filepath = null
    if(req.body.isimage){
        filepath = req.file.path
    }
    const branches = new Branches({
        _id: new mongoose.Types.ObjectId(),
        name:req.body.name,
        address:req.body.address,
        contactNo:req.body.contactNo,
        image: filepath,
        isimage: req.body.isimage
        
    })
    branches.save().then(result=>{

        res.status(200).json({
            message: 'post handler success',
            recivedname:branches.name
        })
    }).catch(err=>{
        res.status(500).json({
            error:err
        })
    });
})

router.get('/:branchId', (req, res, next)=>{
    const id = req.params.branchId;
    Branches.findById(id).exec().then(doc=>{
        if(doc){
            res.status(200).json(doc)
        }else{
            res.status(500).json({error: "ERROR: ID Not found"})
        }
    }).catch(err =>{ 
        res.status(500).json({error: err})
    });
})



router.patch("/:branchId", checkAuth, upload.single('imagefile'), (req, res, next)=>{
    const id = req.params.branchId;
    var filepath = null
    if(req.body.isimage){
        filepath = req.file.path
    }
    const updateOps = {
        name:req.body.name,
        address:req.body.address,
        contactNo:req.body.contactNo,
        image: filepath,
        isimage: req.body.isimage
    };
    Categories.updateOne({_id:id}, {$set: updateOps}).exec().then(result =>{
        res.status(200).json(result);
    }).catch(err=>{
        res.status(500).json({
            error:err
        })
    })
});

router.delete("/:branchId", (req, res, next)=>{
    const id = req.params.branchId;
    Branches.deleteOne({_id:id}).exec().then(result=>{
        res.status(200).json(result); 
    }).catch(err=>{
        res.status(500).json({
            error:err
        });
    });

});

module.exports = router;