const express = require("express");
const router = express.Router();
const mongoose = require("mongoose");
const bcrypt = require("bcrypt");
const User = require("../models/user");
const jwt = require("jsonwebtoken");
const checkAuth = require('../middleware/check-auth');

router.post("/signup", (req, res, next)=>{
    User.findOne({ email:req.body.email }, function (err, docs) {
        if (err){
            res.status(404).json({
                error:err
            })
        }
        else{
            if(docs != null){
                return res.status(409).json({
                    message: 'email already registered'
                })
            }else{
                bcrypt.hash(req.body.password, 10, (err, hash)=>{
                    if(err){
                        return res.status(500).json({
                            error:err
                        })
                    }else{
                        const user = new User({
                            _id: new mongoose.Types.ObjectId(),
                            email: req.body.email,
                            password: hash
                        })
                        user.save()
                        .then(result=>{
                            res.status(201).json({
                                message:'user created'
                            })
                        })
                        .catch(err=>{
                            res.status(500).json({
                                error:err
                            })
                        });
                    }
                })
            }
        }
    });
})

router.post("/login", (req, res, next)=>{
    User.findOne({ email:req.body.email }, function (err, docs) {
        if(err){
            res.status(404).json({
                error:err
            })
        }else{
            if(docs != null){
                bcrypt.compare(req.body.password, docs.password, (err, result)=>{
                    if(err){
                        res.status(401).json({message:"AUTH FAILED"})
                    }else if(result){
                        const token = jwt.sign({
                            email: req.email,
                            userId: docs._id
                        }, 
                        process.env.JWT_KEY, 
                        {
                            expiresIn: "1h"
                        });
                        res.status(200).json({
                            email: docs.email,
                            token: token
                        })
                    }else{
                        res.status(401).json({message:"AUTH FAILED"})
                    }
                })
            }
            else{
                res.status(401).json({message:"AUTH FAILED"})
            }
        }
    })
})

router.get("/islogin", checkAuth, (req, res, next)=>{
    res.status(200).json({
        status:true
    })
})
module.exports = router;

