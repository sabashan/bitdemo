const express = require('express');
const app=express();
const morgan = require('morgan');
const bodyPaser = require('body-parser');
const mongoose = require('mongoose');
const userRoutes = require('./api/routes/user');
const studentRoutes = require('./api/routes/student');
const branchRoutes = require('./api/routes/branch');
const courseRoutes = require('./api/routes/course');
const teacherRoutes = require('./api/routes/teacher');

mongoose.connect(process.env.MONGO_DB_LINK);



app.use(morgan('dev'));
app.use('/uploads',express.static('uploads'));
app.use(express.static('ico'))
app.use(bodyPaser.urlencoded({extended: false}));
app.use(bodyPaser.json());

app.use((req,res,next)=>{
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Credentials', '*')
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization');
    res.header('Access-Control-Allow-Headers', '*');
    if(req.method === 'OPTIONS'){
        res.header('Access-Control-Allow-Method', 'PUT, POST, PATCH, DELETE, GET');
        return res.status(200).json({});
    }
    next();
});


app.use('/branch',branchRoutes);
app.use('/user',userRoutes);
app.use('/student',studentRoutes);
app.use('/course', courseRoutes);
app.use('/teacher', teacherRoutes)

app.use((req, res, next)=>{
    const error = new Error('Not Found');
    error.status(404);
    next(error);
});

app.use((error, req, res, next)=>{
    res.status(error.status|| 500);
    res.json({
        error:{
            message:error.message
        }
    });
});
module.exports = app;